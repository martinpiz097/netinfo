package org.tools.getinfo.services;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.springframework.boot.json.GsonJsonParser;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
public class InfoController {

    //@Autowired -> spring crea instancia automatica de una variable
    // es decir se autoinicializa

    private Gson gson;

    public InfoController() {
        gson = new GsonBuilder()
                .setPrettyPrinting()
                .create();
    }

    @RequestMapping(path = "/")
    public String getInfo(HttpServletRequest request) {
        //return gson.toJson(new RequestInfo(request));
        return "xd";
    }

}
