package org.tools.getinfo.services;

import eu.bitwalker.useragentutils.Browser;
import eu.bitwalker.useragentutils.OperatingSystem;
import eu.bitwalker.useragentutils.UserAgent;

import javax.servlet.http.HttpServletRequest;

public class RequestInfo {
    private final String ip;
    private final String hostName;
    private final int port;
    private final String user;
    private final String browser;
    private final String osName;
    private final int osId;
    private final String manufacturer;
    private final String osGroup;
    private final String deviceType;

    public RequestInfo(HttpServletRequest request) {
        ip = request.getRemoteAddr();
        hostName = request.getRemoteHost();
        port = request.getRemotePort();
        user = request.getRemoteUser();
        UserAgent agent = getAgent(request);
        Browser browser = agent.getBrowser();
        this.browser = browser.getName();
        OperatingSystem os = agent.getOperatingSystem();
        this.osName = os.getName();
        this.osId = os.getId();
        manufacturer = os.getManufacturer().getName();
        osGroup = os.getGroup().getName();
        deviceType = os.getDeviceType().getName();

    }

    private UserAgent getAgent(HttpServletRequest request) {
        return UserAgent
                .parseUserAgentString(request.getHeader("User-Agent"));
    }

    public String getIp() {
        return ip;
    }

    public String getHostName() {
        return hostName;
    }

    public int getPort() {
        return port;
    }

    public String getUser() {
        return user;
    }
}
